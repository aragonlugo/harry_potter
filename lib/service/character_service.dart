import 'dart:convert';

import 'package:harry_potter/model/character.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

//aqui se realiza la coneccion con la api
class CharacterService {
  Future<List<Character>?> fetchCharacters() async {
    List<Character> list = <Character>[];
    http.Response response = await http.get(
        "https://hp-api.herokuapp.com/api/characters"); // especificamente aqui

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      list = json.map<Character>((item) => Character.fromJson(item)).toList();
      return list;
    } else
      return null;
  }
}
