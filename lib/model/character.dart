// ignore_for_file: public_member_api_docs, sort_constructors_first
class Character {
  final String name;
  final String house;
  final bool isStudent;
  final String actor;
  final String image;

  Character({
    required this.name,
    required this.house,
    required this.isStudent,
    required this.actor,
    required this.image,
  });

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
        actor: json["actor"],
        house: json["house"],
        image: json["image"],
        isStudent: json["hogwartsStudent"],
        name: json["name"]);
  }
}
