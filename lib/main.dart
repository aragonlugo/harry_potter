import 'package:flutter/material.dart';
import 'package:harry_potter/view/home.dart';

void main() {
  runApp(MyApp());
}

//pantalla principal de la app
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Home(),
    );
  }
}
